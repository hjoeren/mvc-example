<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"
  trimDirectiveWhitespaces="true"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags"%>
<t:page
  title="${i18n.format(i18n.get('i18n.user', 'editUser'), user.mail)}">
  <h1>${i18n.format(i18n.get('i18n.user', 'editUser'), user.mail)}</h1>
  <form action="${mvc.uri('editUser', {'mail': user.mail})}"
    method="post" novalidate>
    <t:input label="${i18n.get('i18n.user', 'mail')}" type="email"
      id="mail" name="mail" value="${user.mail}" disabled="true" />
    <t:input label="${i18n.get('i18n.user', 'displayName')}" type="text"
      id="displayName" name="displayName" value="${user.displayName}" />
    <t:input label="${i18n.get('i18n.user', 'givenName')}" type="text"
      id="givenName" name="givenName" value="${user.givenName}" />
    <t:input label="${i18n.get('i18n.user', 'surname')}" type="text"
      id="surname" name="surname" value="${user.surname}" />
    <t:input label="${i18n.get('i18n.user', 'birthDate')}" type="date"
      id="birthDate" name="birthDate" value="${user.birthDate}" />
    <input class="btn btn-sm btn-secondary" type="reset"
      value="${i18n.get('i18n.actions', 'cancel')}"> <input
      class="btn btn-sm btn-info" type="submit"
      value="${i18n.get('i18n.actions', 'edit')}">
  </form>
</t:page>