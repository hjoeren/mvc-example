<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"
  trimDirectiveWhitespaces="true"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags"%>
<t:page title="${i18n.get('i18n.user', 'createUser')}">
  <h1>${i18n.get('i18n.user', 'createUser')}</h1>
  <form action="${mvc.uri('createUser')}" method="post" novalidate>
    <t:input label="${i18n.get('i18n.user', 'mail')}" type="email"
      id="mail" name="mail" value="${not empty user ? user.mail : null}" />
    <t:input label="${i18n.get('i18n.user', 'displayName')}" type="text"
      id="displayName" name="displayName"
      value="${not empty user ? user.displayName : null}" />
    <t:input label="${i18n.get('i18n.user', 'givenName')}" type="text"
      id="givenName" name="givenName"
      value="${not empty user ? user.givenName : null}" />
    <t:input label="${i18n.get('i18n.user', 'surname')}" type="text"
      id="surname" name="surname"
      value="${not empty user ? user.surname : null}" />
    <t:input label="${i18n.get('i18n.user', 'birthDate')}" type="date"
      id="birthDate" name="birthDate"
      value="${not empty user ? user.birthDate : null}" />
    <input class="btn btn-sm btn-secondary" type="reset"
      value="${i18n.get('i18n.actions', 'cancel')}"> <input
      class="btn btn-sm btn-primary" type="submit"
      value="${i18n.get('i18n.actions', 'create')}">
  </form>
</t:page>