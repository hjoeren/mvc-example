<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"
  trimDirectiveWhitespaces="true"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags"%>
<t:page
  title="${i18n.format(i18n.get('i18n.user', 'deleteUser'), user.mail)}">
  <h1>${i18n.format(i18n.get('i18n.user', 'deleteUser'), user.mail)}</h1>
  <form action="${mvc.uri('deleteUser', {'mail': user.mail})}"
    method="post">
    <input type="hidden" name="_method" value="DELETE"> <input
      class="btn btn-sm btn-block btn-danger" type="submit"
      value="${i18n.get('i18n.actions', 'delete')}">
  </form>
</t:page>