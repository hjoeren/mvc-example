<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"
  trimDirectiveWhitespaces="true"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags"%>
<t:page title="${i18n.get('i18n.user', 'users')}">
  <h1>${i18n.get('i18n.user', 'users')}</h1>
  <form action="${mvc.uri('getUsers')}">
    <div class="input-group mb-3">
      <input class="form-control form-control-sm" type="search"
        id="search" name="search">
      <div class="input-group-append">
        <input class="btn btn-sm btn-secondary" type="submit"
          value="${i18n.get('i18n.actions', 'search')}">
      </div>
    </div>
  </form>
  <div class="table-responsive">
    <table class="table table-sm">
      <thead class="thead-dark">
        <tr>
          <th>${i18n.get('i18n.user', 'mail')}</th>
          <th>${i18n.get('i18n.user', 'displayName')}</th>
          <th>${i18n.get('i18n.user', 'givenName')}</th>
          <th>${i18n.get('i18n.user', 'surname')}</th>
          <th>${i18n.get('i18n.user', 'birthDate')}</th>
          <th class="text-right">${i18n.get('i18n.actions', 'actions')}</th>
        </tr>
      </thead>
      <tbody>
        <c:forEach var="user" items="${users}">
          <tr>
            <td>${user.mail}</td>
            <td>${user.displayName}</td>
            <td>${user.givenName}</td>
            <td>${user.surname}</td>
            <td>${i18n.formatDate(user.birthDate)}</td>
            <td class="text-right">
              <div class="btn-group btn-group-sm">
                <a class="btn btn-info" role="button"
                  href="${mvc.uri('getEditUser', {'mail': user.mail})}">${i18n.get('i18n.actions', 'edit')}</a>
                <a class="btn btn-danger"
                  href="${mvc.uri('getDeleteUser', {'mail': user.mail})}">${i18n.get('i18n.actions', 'delete')}</a>
              </div>
            </td>
          </tr>
        </c:forEach>
      </tbody>
      <tfoot>
        <tr>
          <td colspan="5" />
          <td class="text-right"><a class="btn btn-sm btn-success"
            role="button" href="${mvc.uri('getCreateUser')}">${i18n.get('i18n.actions', 'create')}</a></td>
        </tr>
      </tfoot>
    </table>
  </div>
</t:page>