<%@tag pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@attribute name="label"%>
<%@attribute name="type"%>
<%@attribute name="id"%>
<%@attribute name="name"%>
<%@attribute name="value"%>
<%@attribute name="disabled" type="java.lang.Boolean"%>
<div class="form-group">
  <c:choose>
    <c:when test="${empty value}">
      <label for="${id}">${label}</label>
      <c:choose>
        <c:when test="${empty disabled or disabled == false}">
          <input
            class="${not empty validationErrors.get(name) ? 'form-control form-control-sm is-invalid' : 'form-control form-control-sm'}"
            type="${type}" id="${id}" name="${name}">
        </c:when>
        <c:otherwise>
          <input
            class="${not empty validationErrors.get(name) ? 'form-control form-control-sm is-invalid' : 'form-control form-control-sm'}"
            type="${type}" id="${id}" name="${name}" disabled>
        </c:otherwise>
      </c:choose>
    </c:when>
    <c:otherwise>
      <label for="${id}">${label}</label>
      <c:choose>
        <c:when test="${empty disabled or disabled == false}">
          <input
            class="${not empty validationErrors.get(name) ? 'form-control form-control-sm is-invalid' : 'form-control form-control-sm'}"
            type="${type}" id="${id}" name="${name}" value="${value}">
        </c:when>
        <c:otherwise>
          <input
            class="${not empty validationErrors.get(name) ? 'form-control form-control-sm is-invalid' : 'form-control form-control-sm'}"
            type="${type}" id="${id}" name="${name}" value="${value}"
            disabled>
        </c:otherwise>
      </c:choose>
    </c:otherwise>
  </c:choose>
  <c:if test="${not empty validationErrors.get(name)}">
    <div class="invalid-feedback">${validationErrors.get(name).message}</div>
  </c:if>
</div>