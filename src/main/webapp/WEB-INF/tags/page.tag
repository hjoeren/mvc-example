<%@tag pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@attribute name="title"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport"
  content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>${not empty title ? title : i18n.get('i18n.app', 'title')}</title>
<link rel="stylesheet"
  href="${pageContext.request.contextPath}/webjars/bootstrap/4.4.1-1/css/bootstrap.min.css">
</head>
<body>
  <t:navigation />
  <div class="container">
    <jsp:doBody />
  </div>
  <script
    src="${pageContext.request.contextPath}/webjars/jquery/3.4.0/jquery.slim.min.js"></script>
  <script
    src="${pageContext.request.contextPath}/webjars/popper.js/1.14.3/umd/popper.min.js"></script>
  <script
    src="${pageContext.request.contextPath}/webjars/bootstrap/4.4.1-1/js/bootstrap.min.js"></script>
</body>
</html>