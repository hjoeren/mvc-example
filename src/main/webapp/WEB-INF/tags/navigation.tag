<%@tag pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<nav class="navbar navbar-dark bg-dark mb-3">
  <div class="container">
    <span class="navbar-brand mb-0 h1">${i18n.get('i18n.app', 'title')}</span>
    <ul class="navbar-nav mr-auto">
      <li class="nav-item"><a class="nav-link"
        href="${mvc.uri('getUsers')}">${i18n.get('i18n.user', 'users')}</a></li>
    </ul>
  </div>
</nav>