package io.gitlab.hjoeren.experimental.mvc.presentation.model;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import javax.enterprise.inject.Model;
import javax.inject.Inject;
import javax.mvc.MvcContext;
import javax.mvc.binding.ValidationError;
import javax.validation.ConstraintViolation;
import javax.validation.MessageInterpolator;
import javax.validation.Path.Node;
import javax.validation.ValidatorFactory;
import javax.validation.metadata.ConstraintDescriptor;

@Model
public class ValidationErrors {

  @Inject
  MvcContext mvcContext;

  @Inject
  ValidatorFactory validatorFactory;

  private Map<String, ValidationError> errors;

  public ValidationError get(String paramName) {
    return errors != null ? errors.get(paramName) : null;
  }

  public ValidationError put(String paramName, ValidationError validationError) {
    if (errors == null) {
      errors = new HashMap<>();
    }
    return errors.put(paramName, validationError);
  }

  public ValidationError put(ConstraintViolation<?> violation) {
    String template = violation.getMessageTemplate();
    MessageInterpolator.Context context = new MessageInterpolatorContextImpl(violation);
    MessageInterpolator interpolator = validatorFactory.getMessageInterpolator();
    String message = interpolator.interpolate(template, context, mvcContext.getLocale());
    ValidationError error = new ValidationErrorImpl(violation, message);
    return put(error.getParamName(), error);
  }

  public void putAll(Collection<ConstraintViolation<?>> violations) {
    for (ConstraintViolation<?> violation : violations) {
      put(violation);
    }
  }

  private static class MessageInterpolatorContextImpl implements MessageInterpolator.Context {

    private ConstraintViolation<?> violation;

    public MessageInterpolatorContextImpl(ConstraintViolation<?> violation) {
      this.violation = violation;
    }

    @Override
    public ConstraintDescriptor<?> getConstraintDescriptor() {
      return violation.getConstraintDescriptor();
    }

    @Override
    public Object getValidatedValue() {
      return violation.getInvalidValue();
    }

    @Override
    public <T> T unwrap(Class<T> type) {
      throw new UnsupportedOperationException();
    }

  }

  private static class ValidationErrorImpl implements ValidationError {

    private ConstraintViolation<?> violation;
    private String message;

    public ValidationErrorImpl(ConstraintViolation<?> violation, String message) {
      this.violation = violation;
      this.message = message;
    }

    @Override
    public String getMessage() {
      return message;
    }

    @Override
    public String getParamName() {
      Node paramNode = null;
      Iterator<Node> iterator = violation.getPropertyPath().iterator();
      while (iterator.hasNext()) {
        paramNode = iterator.next();
      }
      return paramNode.getName();
    }

    @Override
    public ConstraintViolation<?> getViolation() {
      return violation;
    }

  }

}
