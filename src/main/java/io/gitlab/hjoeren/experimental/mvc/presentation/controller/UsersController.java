package io.gitlab.hjoeren.experimental.mvc.presentation.controller;

import javax.inject.Inject;
import javax.mvc.Controller;
import javax.mvc.Models;
import javax.mvc.UriRef;
import javax.mvc.View;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import io.gitlab.hjoeren.experimental.mvc.business.service.UserRepository;

@Path("users")
@Controller
@View("users.jsp")
public class UsersController {

  @Inject
  Models models;

  @Inject
  UserRepository userRepository;

  @GET
  @UriRef("getUsers")
  public void getUsers(@QueryParam("search") String search) {
    if (search == null || search.isBlank()) {
      models.put("users", userRepository.findAll());
    } else {
      models.put("users", userRepository.search(search));
    }
  }

}
