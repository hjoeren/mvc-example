package io.gitlab.hjoeren.experimental.mvc.presentation.controller;

import javax.inject.Inject;
import javax.mvc.Controller;
import javax.mvc.Models;
import javax.mvc.MvcContext;
import javax.mvc.UriRef;
import javax.mvc.View;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;
import io.gitlab.hjoeren.experimental.mvc.business.entity.User;
import io.gitlab.hjoeren.experimental.mvc.business.service.UserRepository;

@Path("users/{mail}/delete")
@Controller
@View("deleteUser.jsp")
public class DeleteUserController {

  @Inject
  MvcContext mvcContext;

  @Inject
  Models models;

  @Inject
  UserRepository userRepository;

  @GET
  @UriRef("getDeleteUser")
  public void getDeleteUser(@PathParam("mail") String mail) {
    models.put("user", userRepository.findByMail(mail));
  }

  @DELETE
  @UriRef("deleteUser")
  public Response deleteUser(@PathParam("mail") String mail) {
    User user = userRepository.findByMail(mail);
    models.put("user", user);
    userRepository.delete(user);
    return Response.status(Response.Status.SEE_OTHER).location(mvcContext.uri("getUsers")).build();
  }

}
