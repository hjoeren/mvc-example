package io.gitlab.hjoeren.experimental.mvc.business.entity;

import java.time.LocalDate;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

@Entity
@Table(name = "USERS")
@NamedQueries({@NamedQuery(name = "users.findAll", query =
    // @formatter:off
      "select u "
    + "from User u"
    // @formatter:on
    ), @NamedQuery(name = "users.findByMailIgnoreCase", query =
    // @formatter:off
      "select u "
    + "from User u "
    + "where upper(u.mail) = upper(:mail)"
    // @formatter:on
    ), @NamedQuery(name = "users.search", query =
    // @formatter:off
      "select u "
    + "from User u "
    + "where upper(u.mail) like upper(:search)"
    + "or upper(u.displayName) like upper(:search) "
    + "or upper(u.givenName) like upper(:search) "
    + "or upper(u.surname) like upper(:search)"
    // @formatter:on
    )})
public class User extends AbstractEntity<Long> {

  @Column(nullable = false, unique = true)
  @NotBlank
  @Email
  private String mail;
  private String displayName;
  @NotBlank
  private String givenName;
  @NotBlank
  private String surname;
  private LocalDate birthDate;

  public User() {}

  public User(String mail, String displayName, String givenName, String surname,
      LocalDate birthDate) {
    this.mail = mail;
    this.displayName = displayName;
    this.givenName = givenName;
    this.surname = surname;
    this.birthDate = birthDate;
  }

  public String getMail() {
    return mail;
  }

  public void setMail(String mail) {
    this.mail = mail;
  }

  public String getDisplayName() {
    return displayName;
  }

  public void setDisplayName(String displayName) {
    this.displayName = displayName;
  }

  public String getGivenName() {
    return givenName;
  }

  public void setGivenName(String givenName) {
    this.givenName = givenName;
  }

  public String getSurname() {
    return surname;
  }

  public void setSurname(String surname) {
    this.surname = surname;
  }

  public LocalDate getBirthDate() {
    return birthDate;
  }

  public void setBirthDate(LocalDate birthDate) {
    this.birthDate = birthDate;
  }

  public void edit(String displayName, String givenName, String surname, LocalDate birthDate) {
    this.displayName = displayName;
    this.givenName = givenName;
    this.surname = surname;
    this.birthDate = birthDate;
  }

}
