package io.gitlab.hjoeren.experimental.mvc.business.entity;

import java.util.Objects;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class AbstractEntity<ID> {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  protected ID id;

  public ID getId() {
    return id;
  }

  protected void setId(ID id) {
    this.id = id;
  }

  public boolean isNew() {
    return id == null;
  }

  @Override
  public int hashCode() {
    return Objects.hash(id);
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (!(obj instanceof AbstractEntity)) {
      return false;
    }
    AbstractEntity<?> other = (AbstractEntity<?>) obj;
    return !isNew() ? Objects.equals(id, other.id) : false;
  }

}
