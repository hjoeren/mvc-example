package io.gitlab.hjoeren.experimental.mvc.presentation.controller;

import java.time.LocalDate;
import javax.inject.Inject;
import javax.mvc.Controller;
import javax.mvc.Models;
import javax.mvc.MvcContext;
import javax.mvc.UriRef;
import javax.mvc.View;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import io.gitlab.hjoeren.experimental.mvc.business.entity.User;
import io.gitlab.hjoeren.experimental.mvc.business.service.UserRepository;
import io.gitlab.hjoeren.experimental.mvc.presentation.validation.Validated;

@Path("users/create")
@Controller
@View("createUser.jsp")
public class CreateUserController {

  @Inject
  MvcContext mvcContext;

  @Inject
  Models models;

  @Inject
  UserRepository userRepository;

  @GET
  @UriRef("getCreateUser")
  public void getCreateUser() {}

  @POST
  @UriRef("createUser")
  @Validated
  public Response createUser(@FormParam("mail") String mail,
      @FormParam("displayName") String displayName, @FormParam("givenName") String givenName,
      @FormParam("surname") String surname, @FormParam("birthDate") LocalDate birthDate) {
    User user = new User(mail, displayName, givenName, surname, birthDate);
    models.put("user", user);
    userRepository.save(user);
    return Response.status(Response.Status.SEE_OTHER).location(mvcContext.uri("getUsers")).build();
  }

  // @POST
  // @UriRef("createUser")
  // @Validated
  // public String createUser(@FormParam("mail") String mail,
  // @FormParam("displayName") String displayName, @FormParam("givenName") String givenName,
  // @FormParam("surname") String surname, @FormParam("birthDate") LocalDate birthDate) {
  // User user = new User(mail, displayName, givenName, surname, birthDate);
  // models.put("user", user);
  // userRepository.save(user);
  // return "redirect:/users";
  // }

}
