package io.gitlab.hjoeren.experimental.mvc.presentation.controller;

import java.time.LocalDate;
import javax.inject.Inject;
import javax.mvc.Controller;
import javax.mvc.Models;
import javax.mvc.MvcContext;
import javax.mvc.UriRef;
import javax.mvc.View;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;
import io.gitlab.hjoeren.experimental.mvc.business.entity.User;
import io.gitlab.hjoeren.experimental.mvc.business.service.UserRepository;
import io.gitlab.hjoeren.experimental.mvc.presentation.validation.Validated;

@Path("users/{mail}/edit")
@Controller
@View("editUser.jsp")
public class EditUserController {

  @Inject
  MvcContext mvc;

  @Inject
  Models models;

  @Inject
  UserRepository userRepository;

  @GET
  @UriRef("getEditUser")
  public void getEditUser(@PathParam("mail") String mail) {
    models.put("user", userRepository.findByMail(mail));
  }

  @POST
  @UriRef("editUser")
  @Validated
  public Response editUser(@PathParam("mail") String mail,
      @FormParam("displayName") String displayName, @FormParam("givenName") String givenName,
      @FormParam("surname") String surname, @FormParam("birthDate") LocalDate birthDate) {
    User user = userRepository.findByMail(mail);
    user.edit(displayName, givenName, surname, birthDate);
    models.put("user", user);
    userRepository.save(user);
    return Response.status(Response.Status.SEE_OTHER).location(mvc.uri("getUsers")).build();
  }

}
