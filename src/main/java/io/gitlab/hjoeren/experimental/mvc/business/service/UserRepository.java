package io.gitlab.hjoeren.experimental.mvc.business.service;

import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import javax.enterprise.context.RequestScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import javax.validation.Valid;
import io.gitlab.hjoeren.experimental.mvc.business.entity.User;

@RequestScoped
@Transactional
public class UserRepository {

  @PersistenceContext
  EntityManager entityManager;

  public List<User> findAll() {
    TypedQuery<User> query = entityManager.createNamedQuery("users.findAll", User.class);
    return query.getResultList();
  }

  public Optional<User> findOptionalByMail(String mail) {
    TypedQuery<User> query =
        entityManager.createNamedQuery("users.findByMailIgnoreCase", User.class);
    query.setParameter("mail", mail);
    return query.getResultStream().findFirst();
  }

  public User findByMail(String mail) {
    return findOptionalByMail(mail).get();
  }

  public List<User> search(String search) {
    TypedQuery<User> query = entityManager.createNamedQuery("users.search", User.class);
    query.setParameter("search", String.format("%%%s%%", search));
    return query.getResultList();
  }

  public User save(@Valid User entity) {
    Optional<User> optionalDuplicate = findOptionalByMail(entity.getMail());

    if (optionalDuplicate.filter(Predicate.not(entity::equals)).isPresent()) {
      // TODO exception handling on duplicates
      throw new RuntimeException();
    }

    if (entity.isNew()) {
      entityManager.persist(entity);
    }

    return entityManager.merge(entity);
  }

  public void delete(User entity) {
    if (!entityManager.contains(entity)) {
      entity = entityManager.merge(entity);
    }

    entityManager.remove(entity);
  }

}
