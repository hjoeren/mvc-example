package io.gitlab.hjoeren.experimental.mvc.presentation.validation;

import java.lang.reflect.Method;
import javax.annotation.Priority;
import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;
import javax.mvc.View;
import javax.validation.ConstraintViolationException;
import javax.ws.rs.core.Response;
import io.gitlab.hjoeren.experimental.mvc.presentation.model.ValidationErrors;

@Priority(Interceptor.Priority.PLATFORM_AFTER + 1000)
@Validated
@Interceptor
public class ValidationInterceptor {

  @Inject
  ValidationErrors validationErrors;

  private View resolveView(Method method) {
    return method.getAnnotation(View.class) != null ? method.getAnnotation(View.class)
        : method.getDeclaringClass().getAnnotation(View.class);
  }

  private ConstraintViolationException resolveException(Throwable throwable) {
    Throwable cause = throwable;
    do {
      if (cause instanceof ConstraintViolationException) {
        return (ConstraintViolationException) cause;
      }
    } while ((cause = cause.getCause()) != null);

    return null;
  }

  @AroundInvoke
  public Object aroundInvoke(InvocationContext context) throws Exception {
    try {
      return context.proceed();
    } catch (Throwable throwable) {
      ConstraintViolationException violationException = resolveException(throwable);
      if (violationException == null) {
        throw throwable;
      }
      View view = resolveView(context.getMethod());
      if (view == null) {
        throw throwable;
      }
      validationErrors.putAll(violationException.getConstraintViolations());
      if (context.getMethod().getReturnType().equals(Response.class)) {
        return Response.status(Response.Status.BAD_REQUEST).entity(view.value()).build();
      } else if (context.getMethod().getReturnType().equals(String.class)) {
        return view.value();
      } else {
        return null;
      }
    }
  }

}
