package io.gitlab.hjoeren.experimental.mvc.presentation.model;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.logging.Logger;
import javax.enterprise.inject.Model;
import javax.inject.Inject;
import javax.mvc.MvcContext;

@Model
public class I18n {

  private static final Logger LOGGER = Logger.getLogger(I18n.class.getName());

  private static final String DEFAULT_BASE_NAME = "i18n";

  @Inject
  MvcContext mvcContext;

  public String get(String baseName, String key) {
    try {
      ResourceBundle bundle = ResourceBundle.getBundle(baseName, mvcContext.getLocale());

      return bundle.getString(key);
    } catch (MissingResourceException e) {
      LOGGER.warning(e.getMessage());

      return String.format("???%s.%s???", baseName, key);
    }
  }

  public String get(String key) {
    return get(I18n.DEFAULT_BASE_NAME, key);
  }

  public String format(String format, Object... args) {
    return String.format(format, args);
  }

  public String formatDateTime(String dateTimeString) {
    DateTimeFormatter formatter = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.SHORT);
    return dateTimeString != null && !dateTimeString.isBlank()
        ? formatter.withLocale(mvcContext.getLocale()).format(LocalDateTime.parse(dateTimeString))
        : null;
  }

  public String formatDate(String dateString) {
    DateTimeFormatter formatter = DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT);
    return dateString != null && !dateString.isBlank()
        ? formatter.withLocale(mvcContext.getLocale()).format(LocalDate.parse(dateString))
        : null;
  }

  public String formatTime(String timeString) {
    DateTimeFormatter formatter = DateTimeFormatter.ofLocalizedTime(FormatStyle.SHORT);
    return timeString != null && !timeString.isBlank()
        ? formatter.withLocale(mvcContext.getLocale()).format(LocalTime.parse(timeString))
        : null;
  }

}
