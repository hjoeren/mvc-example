package io.gitlab.hjoeren.experimental.mvc.presentation.parameter;

import java.time.LocalDate;
import javax.ws.rs.ext.ParamConverter;

public class LocalDateParamConverter implements ParamConverter<LocalDate> {

  @Override
  public LocalDate fromString(String value) {
    return value != null && !value.isBlank() ? LocalDate.parse(value) : null;
  }

  @Override
  public String toString(LocalDate value) {
    return value != null ? value.toString() : null;
  }

}
