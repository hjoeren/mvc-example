package io.gitlab.hjoeren.experimental.mvc.presentation;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("mvc")
public class MvcConfiguration extends Application {

}
